<?php
    include('function.php');
    get_header();
?>
<header>
    <div class="Header">
        <div class="Header__menu"><a class="Header__logotype" href="#"></a>
            <button class="Burger__Icon" type="button"></button>
            <div class="Burger Burger__shown">
                <a class="Header__menuItem" href="<?php get_the_title(82); ?>"><?php echo get_the_title(82); ?></a>
                <a class="Header__menuItem" href="<?php get_the_title($page_id); ?>"><?php echo get_the_title($page_id); ?></a>
                <a class="Header__menuItem" href="<?php get_the_title(87); ?>"><?php echo get_the_title(87); ?></a>
                <a class="Header__menuItem" href="<?php get_the_title(89); ?>"><?php echo get_the_title(89); ?></a>
            </div>
        </div><a class="Header__cart" href="#">
            <div class="Header__cartBuy"></div></a>
    </div>
</header>
<div class="content">
  <?php
      $page_id = get_option('page_on_front');
      $content_post = get_post($page_id);
      $content = $content_post->post_content;
      $content = apply_filters('the_content', $content);
      $content = str_replace(']]>', ']]&gt;', $content);
  ?>
      <section>
        <div class="ContentMenu">
            <?php
                  $categories = get_terms('category', 'orderby=name&hide_empty=0');
                  $category_link_array = array();
                  if($categories) {
                    foreach ($categories as $cat) {
                      echo '<a href="'. get_category_link($single_cat->term_id) .'" class="ContentMenu__Link">' .$cat->name .'</a>';
                    }
                  }
                ?>
        </div>
      </section>
      <?php
        $s0 = '';
        $s1 = '';
        $s2 = '';
        $s3 = '';
        
      $order = array ('orderby' => 'date', 'order' => 'ASC');
      
      if(isset($_GET['select'])) {
          switch($_GET['select']) {
            case "date":
                        $order = array ('orderby' => 'date', 'order' => 'ASC');
                        $s0 = ' selected="selected"';
                        break;
            case "title":
                        $order = array ( 'orderby' => 'title', 'order' => 'ASC' ); 
                        $s1 = ' selected="selected"';
                        break;
            case "price":
                        $order = array ( 'orderby' => 'meta_value_num', 'meta_key' => 'price_for_one');
                        $s2 = ' selected="selected"';
                        break;
            case "popularity":
                        $order = array ( 'orderby' => 'meta_value', 'meta_key' => 'popular');
                        $s3 = ' selected="selected"';
                        break;
            }
        }
      ?>
      <section>
        <div class="Sorting">
          <?= $content; ?>
          <form class="Sorting__Sel"><span>Sorted by </span>
            <select class="Sorting__Select" name="sorting">
              <option value="date"<?= $s0 ?>>date</option>
              <option value="title" <?= $s1 ?>>title</option>
              <option value="price" <?= $s2 ?>>price</option>
              <option value="popularity" <?= $s3 ?>>popularity</option>
            </select>
          </form>
        </div>
      </section>
      <?php 
        global $query_string; // параметры базового запроса
        query_posts($query_string.'&'.$order); // базовый запрос + свои параметры
      ?>
      <section>
        <div class="SoupCards">
            <?php
                    $args = array(
                        'posts_per_page' => -1,
                    );
                    $args = array_merge($args, $order);
                    $category_posts = new WP_Query($args);

                    if ($category_posts->have_posts()):
                        while($category_posts->have_posts()):
                            $category_posts -> the_post();
                            $image_url = get_the_post_thumbnail_url( get_the_ID(), 'full');
                            $price_for_one = get_post_meta(get_the_ID(), 'price_for_one', true);
                            $price_for_two = get_post_meta(get_the_ID(), 'price_for_two', true);
                            $count = get_post_meta(get_the_ID(), 'quantity', true);
            ?>
                <div class="SoupCards__Frame">
                  <?php if($count > 0) {
                    echo '<div class="SoupCards__Animated">';
                      echo '<div class="top"></div>';
                      echo '<div class="bottom"></div>';
                      echo '<div class="count"></div>';
                    echo '</div>'; 
                    echo '<div class="SoupCards__Circle">';
                      if($price_for_two == 0) {
                        echo '<div class=SoupCards__Image style="background-image: url('. $image_url .'")></div>';
                      } else {
                        echo '<div class=SoupCards__SallImage></div>';
                        echo '<div class=SoupCards__Image style="background-image: url('. $image_url .'")></div>';
                      }
                    echo '</div>';   
                    echo '<div class="SoupCards__Info">';
                      echo '<h3 class=SoupCards__Title>';
                        echo the_title(); 
                      echo '</h3>';
                      echo '<div class="SoupCards__Text">'. the_content(). '</div>';
                      echo '<div class="SoupCards__PriceBlock">';
                            if($price_for_two == 0) {
                              echo '<button class="SoupCards__Price">$'. number_format($price_for_one, 2, ',', ' ').' for one</button>';
                            } else {
                              echo '<button class="SoupCards__Price">$'. number_format($price_for_one, 2, ',', ' ').' for one</button>';
                              echo '<button class="SoupCards__Price SoupCards__SalePrice">$'. number_format($price_for_two, 2, ',', ' '). ' for 2 soups</button>';
                            }
                          echo '<button class="remove">Remove from cart</button>';
                        echo '</div>';
                      echo '</div>';
                    } else {
                        echo '<div class="SoupCards__AllSoldFrame">';
                          echo '<div class="SoupCards__AllSoldInfo">';
                            echo '<h3 class="SoupCards__AllSoldTitle">'.get_the_title(91). '</h3>';
                            echo '<div class="SoupCards__AllSoldText">'. get_the_content(91) .'</div>';
                          echo '</div>';
                        echo '</div>';
                    } 
                  ?>
                  </div>
            <?php endwhile;
                else:
                    echo "no posts";
                endif; 
            ?>
        </div>
    </section>
      <?php require_once('Form.php'); ?>
</div>
<?php
    get_footer();
?>