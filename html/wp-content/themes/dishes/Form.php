<section>
    <div class="Form">
        <div class="Form__text">
        <h2>Want something specific?</h2>
        <p>We can cook any soup for you. Even not listed up here. Just message us. </p>
        <form class="Form__Comment">
            <label class="Form__ItemComment"><span class="Form__Span">Soup name and short description</span>
            <textarea class="Textarea" name="comment" rows="4"></textarea>
            </label>
            <label class="Form__ItemEmail"><span class="Form__Span">Email</span>
            <input class="Form__Input" type="email" name="email"/>
            </label>
            <input class="Form_Button" type="submit" value="Send" disabled="disabled" name="button"/>
        </form>
        <div class="Form__Image"></div>
        </div>
    </div>
</section>