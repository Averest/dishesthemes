<?php
    wp_footer();
?>
<div class="footer-container">
    <footer>
        <div class="Footer">
            <div class="Footer__line">
                <div class="Footer__ItemLeft"><span class="Footer__Span">© 2017, Sup Soup</span>
                    <div class="FooterItem__Link"><a class="Footer__link" href="#">Contact support</a>
                    </div>
                    <div class="FooterItem__Link"><a class="Footer__link" href="#">Work in Sup Soup</a>
                    </div>
                    <div class="FooterItem__Link">
                    </div>
                </div>
                <div class="Footer__ItemRight">
                    <a class="Footer__LinkImage" href="#">Design by</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</body>
</html>