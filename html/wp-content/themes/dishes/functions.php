<?php
define('TEST_VERSION', '0.0.1');

function form_request_function() {
    $result = [
        'status' => 'success',
        'errors' => [],
    ];

    $to = get_option('admin_email');
    $subject = 'Test Form Request';
    $body = json_encode($_POST, JSON_UNESCAPED_UNICODE);
    $headers = array('Content-Type: text/html; charset=UTF-8');

    wp_mail( $to, $subject, $body, $headers );

    echo json_encode($result);
    die();
}
add_action('wp_ajax_form_request', 'form_request_function');
add_action('wp_ajax_nopriv_form_request', 'form_request_function');


add_action( 'wp_enqueue_scripts', function() {
    wp_deregister_script('jquery');
    wp_enqueue_script( 'theme_script', get_template_directory_uri() . '/bundle.js', [], TEST_VERSION, true );
    wp_enqueue_script( 'stretchy_script', get_template_directory_uri() . '/stretchy.js', [], TEST_VERSION, true );
    wp_enqueue_style( 'build_style', get_template_directory_uri() . '/build.css', [], TEST_VERSION );
    wp_localize_script( 'theme_script', 'testAjaxObject',
            array(
                'ajaxurl' => admin_url( 'admin-ajax.php' ),
            ));
} );

add_theme_support( 'post-thumbnails', array( 'post' ) );