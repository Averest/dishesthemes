<section>
        <form class="Form">
            <label class="Input">
                <span class="Input__name">Телефон</span>
                <span class="Input__body">
                    <input class="Input__line" size="1" type="tel" placeholder="+7 900 123-45-67"/>
                    <i class="Input__icon fa fa-edit"></i>
                </span>
            </label>
            <label class="Input">
                <span class="Input__name">Почта</span>
                <span class="Input__body">
                    <input class="Input__line" size="1" type="email" placeholder="out@email.com"/>
                    <i class="Input__icon fa fa-envelope"></i>
                </span>
            </label>
            <label class="Input">
                <span class="Input__name">Количество</span>
                <span class="Input__body">
                    <input class="Input__line" size="1" type="number" placeholder="20" min="0" max="20" step="1"/>
                    <i class="Input__icon fa fa-crosshairs"></i>
                </span>
            </label>
            <button class="button" type="submit" disabled="disabled">Отправить</button>
        </form>
    </section>