<?php
    include("function.php");
    get_header();
    
?>
<header>
    <div class="Logotype">
        <a class="Logotype__image" href="https://osagent.ru"></a>
    </div>
</header>
<div class="content">
    <?php
        $page_id = get_option('page_on_front');
        $content_post = get_post($page_id);
        $content = $content_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
    ?>
    <section>
        <h1><?= get_the_title($page_id); ?></h1>
    </section>
    <section>
        <?= $content; ?>
    </section>
    <section>
        <div class="Card">
            <?php
                $args = array(
                    'posts_per_page' => -1,
                    'orderby' => 'comment_count'
                );
                $category_posts = new WP_Query($args);

                if ($category_posts->have_posts()):
                    while($category_posts->have_posts()):
                        $category_posts -> the_post();
            ?>
                    <div class="Card-item">
                        <div class="Card-frame">
                            <div class="Card-circle"></div>
                            <div class="Card-text"><?php the_content(); ?></div>
                        </div>
                    </div>
            <?php endwhile;
                else:
                    echo "no posts";
                endif; 
            ?>
        </div>
    </section>
    <section>
        <h2>Формы</h2>
        <p>Клик на название поля должен его активировать. Все поля обязательные. Конпка становится активной после заполнения всех полей. Состояния кнопки на артборде с компонентами слева.</p>
    </section>
    <?php require_once('Form.php'); ?>
</div>
<?php
    get_footer();
?>
